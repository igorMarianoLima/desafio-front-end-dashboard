import { Injectable } from '@angular/core';
import { faker } from '@faker-js/faker'

import { Buy, BuyStatus } from '../../components/tables/table-buyers/table.buyers.interface';

@Injectable({
  providedIn: 'root'
})
export class BuyerService {

  constructor() { }

  generateRandomBuy(): Buy {
    const possibleBuyStatus: BuyStatus[] = ['APPROVED', 'IN ANALYSIS'];

    return {
      id: faker.number.int({
        min: 1,
        max: 999999,
      }),
      date: faker.date.recent(),
      name: faker.person.fullName(),
      location: faker.location.state(),
      status: faker.helpers.arrayElement(possibleBuyStatus),
      value: Number(faker.commerce.price())
    }
  }
}
