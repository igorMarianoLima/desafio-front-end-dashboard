import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {

  constructor() { }

  getBalanceGrownPercentage({
    newBalance,
    oldBalance
  }: {
    newBalance: number,
    oldBalance: number
  }): number {
    const percentage = Math.round((newBalance - oldBalance) / oldBalance * 100);
    return isNaN(percentage) ? 0 : percentage;
  }
}
