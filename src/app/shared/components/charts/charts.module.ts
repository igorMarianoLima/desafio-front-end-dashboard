import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartStackModule } from './chart-stack/chart-stack.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ChartStackModule
  ],
  exports: [
    ChartStackModule
  ]
})
export class ChartsModule { }
