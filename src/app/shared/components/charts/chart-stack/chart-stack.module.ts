import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartStackComponent } from './chart-stack.component';



@NgModule({
  declarations: [
    ChartStackComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ChartStackComponent
  ]
})
export class ChartStackModule { }
