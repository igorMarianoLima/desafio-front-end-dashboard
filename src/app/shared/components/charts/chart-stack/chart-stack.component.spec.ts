import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartStackComponent } from './chart-stack.component';

describe('ChartStackComponent', () => {
  let component: ChartStackComponent;
  let fixture: ComponentFixture<ChartStackComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChartStackComponent]
    });
    fixture = TestBed.createComponent(ChartStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
