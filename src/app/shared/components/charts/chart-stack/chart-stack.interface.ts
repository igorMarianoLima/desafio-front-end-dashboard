export interface Stack {
    percentage: number;
    label: string;
    active?: boolean;
}

export interface ChartStack {
    title: string;
    verticalLabels: any[];
    stacks: Stack[];
}

