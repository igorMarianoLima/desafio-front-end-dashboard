import { Component, Input } from '@angular/core';

import { ChartStack } from './chart-stack.interface';

@Component({
  selector: 'chart-stack',
  templateUrl: './chart-stack.component.html',
  styleUrls: ['./chart-stack.component.scss']
})
export class ChartStackComponent {
  @Input() title: ChartStack['title'] = 'Gráfico de pilhas'
  @Input() verticalLabels: ChartStack['verticalLabels'] = []
  @Input() stacks: ChartStack['stacks'] = [];
}
