import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableBuyersComponent } from './table-buyers.component';

describe('TableBuyersComponent', () => {
  let component: TableBuyersComponent;
  let fixture: ComponentFixture<TableBuyersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableBuyersComponent]
    });
    fixture = TestBed.createComponent(TableBuyersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
