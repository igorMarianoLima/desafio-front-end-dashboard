export type BuyStatus = 'APPROVED' | 'IN ANALYSIS'

export interface Buy {
    id: number;
    date: Date;
    name: string;
    location: string;
    value: number;
    status: BuyStatus;
}

export interface TableBuyers {
    buyers: Buy[];
}