import { Component, Input } from '@angular/core';
import { Buy, BuyStatus, TableBuyers } from './table.buyers.interface';

@Component({
  selector: 'table-buyers',
  templateUrl: './table-buyers.component.html',
  styleUrls: ['./table-buyers.component.scss']
})
export class TableBuyersComponent {
  @Input() buyers: TableBuyers['buyers'] = [];

  fieldsNameSerialized: Record<keyof Buy, string> = {
    id: 'ID',
    date: 'Data',
    name: 'Nome',
    location: 'Localização',
    value: 'Valor',
    status: 'Situação',
  }
  fieldsName = Object.values(this.fieldsNameSerialized)

  statusValueSerialized: Record<BuyStatus, {
    color: string,
    label: string
  }> = {
    'IN ANALYSIS': {
      color: 'var(--support-03)',
      label: 'Em análise'
    },
    'APPROVED': {
      color: 'var(--support-02)',
      label: 'Aprovado'
    }
  }
}
