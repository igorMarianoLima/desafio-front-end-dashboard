import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableBuyersComponent } from './table-buyers.component';



@NgModule({
  declarations: [
    TableBuyersComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TableBuyersComponent
  ]
})
export class TableBuyersModule { }
