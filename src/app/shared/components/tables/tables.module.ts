import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableBuyersModule } from './table-buyers/table-buyers.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TableBuyersModule
  ],
  exports: [
    TableBuyersModule
  ]
})
export class TablesModule { }
