export type AvatarSize = 42;

export interface Avatar {
    src: string;
    size: AvatarSize;
}