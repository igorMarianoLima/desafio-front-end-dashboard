import { Component, Input } from '@angular/core';

import { Avatar } from './avatar.interface';

@Component({
  selector: 'user-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent {
  @Input() src: Avatar['src'] = '';
  @Input() size: Avatar['size'] = 42;
}
