import { Component, HostBinding, Input, ViewChild, ViewContainerRef } from '@angular/core';

import { ICONS } from './icons';
import { IconName, IconSize } from './icon.interface';

@Component({
  selector: 'icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent {
  @Input() name: IconName = 'grid'
  
  @HostBinding('style.--size')
  @Input() size: IconSize = 24;

  @HostBinding('style.--fill')
  @Input() fill?: string;

  @HostBinding('style.--stroke')
  @Input() stroke?: string;

  @ViewChild('iconHost', {read: ViewContainerRef})
  iconHost!: ViewContainerRef;

  ngAfterViewInit() {
    this.iconHost.clear();

    if (!ICONS[this.name]) {
      throw new Error(`Icon ${this.name} does not exist`)
    }

    this.iconHost.createComponent(ICONS[this.name])
  }
}
