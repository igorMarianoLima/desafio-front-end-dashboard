import { ArrowLowIconComponent } from './arrow-low/arrow-low-icon.component'
import { ArrowUpIconComponent } from './arrow-up/arrow-up-icon.component'
import { BellIconComponent } from './bell-icon/bell-icon.component'
import { GridIconComponent } from './grid-icon/grid-icon.component'
import { ListIconComponent } from './list-icon/list-icon.component'
import { SaveIconComponent } from './save-icon/save-icon.component'
import { SearchIconComponent } from './search-icon/search-icon.component'
import { ShoppingCartIconComponent } from './shopping-cart-icon/shopping-cart-icon.component'
import { UsersIconComponent } from './users-icon/users-icon.component'

const ICONS = {
    'grid': GridIconComponent,
    'save': SaveIconComponent,
    'shopping-cart': ShoppingCartIconComponent,
    'users': UsersIconComponent,
    'arrow-up': ArrowUpIconComponent,
    'arrow-low': ArrowLowIconComponent,
    'search': SearchIconComponent,
    'bell': BellIconComponent,
    'list': ListIconComponent
}

export {
    ICONS
}