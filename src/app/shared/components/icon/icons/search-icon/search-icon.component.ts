import { Component } from '@angular/core';

@Component({
  selector: 'icon-search',
  templateUrl: './search-icon.svg',
  styleUrls: ['../icons.scss']
})
export class SearchIconComponent {

}
