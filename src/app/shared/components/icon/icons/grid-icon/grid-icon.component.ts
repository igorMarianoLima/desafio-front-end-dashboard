import { Component } from '@angular/core';

@Component({
  selector: 'icon-grid',
  templateUrl: './grid-icon.svg',
  styleUrls: ['../icons.scss']
})
export class GridIconComponent {

}
