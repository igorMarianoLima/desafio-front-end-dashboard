import { Component } from '@angular/core';

@Component({
  selector: 'icon-users',
  templateUrl: './users-icon.svg',
  styleUrls: ['../icons.scss']
})
export class UsersIconComponent {

}
