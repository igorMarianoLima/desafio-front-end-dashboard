import { Component } from '@angular/core';

@Component({
  selector: 'icon-save',
  templateUrl: './save-icon.svg',
  styleUrls: ['../icons.scss']
})
export class SaveIconComponent {

}
