import { Component } from '@angular/core';

@Component({
  selector: 'icon-shopping-cart',
  templateUrl: './shopping-cart-icon.svg',
  styleUrls: ['../icons.scss']
})
export class ShoppingCartIconComponent {

}
