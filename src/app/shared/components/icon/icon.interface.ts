import { ICONS } from './icons';

export type IconName = keyof typeof ICONS;

export type IconSize = 48 | 32 | 24 | 16