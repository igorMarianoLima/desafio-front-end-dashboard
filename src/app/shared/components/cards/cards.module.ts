import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardResumeModule } from './card-resume/card-resume.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CardResumeModule
  ],
  exports: [
    CardResumeModule
  ]
})
export class CardsModule { }
