export interface CardResume {
    title: string;
    currentBalance: number;
    oldBalance: number;
}