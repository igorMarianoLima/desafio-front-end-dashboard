import { Component, Input } from '@angular/core';
import { BalanceService } from 'src/app/shared/services/balance/balance.service';
import { CardResume } from './card-resume.interface';

@Component({
  selector: 'card-resume',
  templateUrl: './card-resume.component.html',
  styleUrls: ['./card-resume.component.scss']
})
export class CardResumeComponent {
  @Input() title: CardResume['title'] = 'Resumo';
  @Input() currentBalance: CardResume['currentBalance'] = 0;
  @Input() oldBalance: CardResume['oldBalance'] = 0;

  grownPercentage = 0;

  constructor(
    private balanceService: BalanceService
  ) {}

  ngOnInit() {
    this.grownPercentage = this.balanceService.getBalanceGrownPercentage({
      newBalance: this.currentBalance,
      oldBalance: this.oldBalance
    })
  }
}
