import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardResumeComponent } from './card-resume.component';
import { IconModule } from '../../icon/icon.module';



@NgModule({
  declarations: [
    CardResumeComponent
  ],
  imports: [
    CommonModule,
    IconModule
  ],
  exports: [
    CardResumeComponent
  ]
})
export class CardResumeModule { }
