export interface Log {
    label: string;
    value: string;
}

export interface Logs {
    title: string;
    logs: Log[]
}