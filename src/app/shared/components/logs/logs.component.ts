import { Component, Input } from '@angular/core';
import { Logs } from './logs.interface';

@Component({
  selector: 'logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent {
  @Input() title: Logs['title'] = 'Histórico';
  @Input() logs: Logs['logs'] = [];
}
