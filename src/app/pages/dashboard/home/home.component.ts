import { Component } from '@angular/core';
import { faker } from '@faker-js/faker'

import { BuyerService } from 'src/app/shared/services/buyer/buyer.service';

import { CardResume } from 'src/app/shared/components/cards/card-resume/card-resume.interface';
import { ChartStack } from 'src/app/shared/components/charts/chart-stack/chart-stack.interface';
import { Logs } from 'src/app/shared/components/logs/logs.interface';
import { Buy } from 'src/app/shared/components/tables/table-buyers/table.buyers.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  recipeChart: ChartStack = {
    title: 'Gráfico de receitas',
    verticalLabels: [0, 250, 500, 1000, 2000],
    stacks: [
      {
        label: 'Fev',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      {
        label: 'Mar',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      {
        label: 'Abr',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      {
        label: 'Mai',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      { 
        label: 'Jun',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      {
        label: 'Jul',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        })
      },
      {
        label: 'Ago',
        percentage: faker.number.float({
          min: 0.01,
          max: 100
        }),
        active: true
      }
    ]
  }

  transactionsHistory: Logs = {
    title: 'Histórico de Transações',
    logs: [
      {
        label: 'Ontem',
        value: faker.finance.amount({symbol: 'R$'})
      },
      {
        label: 'Quinta-feira',
        value: faker.finance.amount({symbol: 'R$'})
      },
      {
        label: 'Segunda-feira',
        value: faker.finance.amount({symbol: 'R$'})
      }
    ]
  }

  resumes: CardResume[] = [
    {
      title: 'Total de vendas',
      currentBalance: Number(faker.finance.amount()),
      oldBalance: Number(faker.finance.amount())
    },
    {
      title: 'Total líquido',
      currentBalance: Number(faker.finance.amount()),
      oldBalance: Number(faker.finance.amount())
    },
    {
      title: 'Compras canceladas',
      currentBalance: Number(faker.finance.amount()),
      oldBalance: Number(faker.finance.amount())
    },
    {
      title: 'Reembolsos',
      currentBalance: Number(faker.finance.amount()),
      oldBalance: Number(faker.finance.amount())
    },
  ]

  buyers: Buy[] = []

  constructor(
    private buyerService: BuyerService
  ) {}

  ngOnInit() {
    this.generateRandomBuyers()
  }

  generateRandomBuyers() {
    const buyersQuantity = Math.ceil(Math.random() * 12)

    for (let index = 0; index < buyersQuantity; index++) {
      this.buyers[index] = this.buyerService.generateRandomBuy()
    }
  }
}
