import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CoreModule } from 'src/app/core/core.module';

import { ChartsModule } from 'src/app/shared/components/charts/charts.module';
import { LogsModule } from 'src/app/shared/components/logs/logs.module';
import { CardResumeModule } from 'src/app/shared/components/cards/card-resume/card-resume.module';
import { TableBuyersModule } from 'src/app/shared/components/tables/table-buyers/table-buyers.module';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CoreModule,
    ChartsModule,
    LogsModule,
    CardResumeModule,
    TableBuyersModule
  ]
})
export class HomeModule { }
