import { Component } from '@angular/core';

@Component({
  selector: 'dashboard-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  hasNotification = !!Math.round(Math.random())
  avatarSrc = 'https://avatars.githubusercontent.com/u/63012948';
}
