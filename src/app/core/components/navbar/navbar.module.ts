import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';

import { IconModule } from 'src/app/shared/components/icon/icon.module';
import { AvatarModule } from 'src/app/shared/components/avatar/avatar.module';



@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    IconModule,
    AvatarModule
  ],
  exports: [
    NavbarComponent
  ]
})
export class NavbarModule { }
