import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { RouterModule } from '@angular/router';
import { NavItemComponent } from './nav-item/nav-item.component';
import { IconModule } from 'src/app/shared/components/icon/icon.module';



@NgModule({
  declarations: [
    SidebarComponent,
    NavItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconModule
  ],
  exports: [
    SidebarComponent
  ]
})
export class SidebarModule { }
