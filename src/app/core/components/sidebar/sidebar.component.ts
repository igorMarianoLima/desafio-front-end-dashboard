import { Component } from '@angular/core';

import { NavItem } from './nav-item/nav-item.interface';

@Component({
  selector: 'dashboard-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  links: NavItem[] = [
    {
      icon: 'grid',
      label: 'Dashboard',
      redirectTo: '/dashboard'
    },
    {
      icon: 'save',
      label: 'Carteira',
      redirectTo: '/dashboard/carteira'
    },
    {
      icon: 'shopping-cart',
      label: 'Transações',
      redirectTo: '/dashboard/transacoes'
    },
    {
      icon: 'users',
      label: 'Suporte',
      redirectTo: '/dashboard/suporte'
    }
  ]
}
