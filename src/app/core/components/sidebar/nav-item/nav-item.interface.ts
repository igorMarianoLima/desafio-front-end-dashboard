import { IconName } from "src/app/shared/components/icon/icon.interface";

export interface NavItem {
    icon: IconName;
    label: string;
    redirectTo: string;
}