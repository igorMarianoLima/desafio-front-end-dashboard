import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NavItem } from './nav-item.interface';

@Component({
  selector: 'sidebar-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent {
  @Input() icon: NavItem['icon'] = 'grid';
  @Input() label: NavItem['label'] = 'Dashboard';
  @Input() redirectTo: NavItem['redirectTo'] = '/dashboard';

  isActive = false;

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.isActive = this.router.url === this.redirectTo
  }
}
