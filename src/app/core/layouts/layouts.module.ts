import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';

import { SidebarModule } from '../components/sidebar/sidebar.module';
import { NavbarModule } from '../components/navbar/navbar.module';



@NgModule({
  declarations: [
    DefaultLayoutComponent
  ],
  imports: [
    CommonModule,
    SidebarModule,
    NavbarModule
  ],
  exports: [
    DefaultLayoutComponent
  ]
})
export class LayoutsModule { }
