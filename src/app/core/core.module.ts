import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsModule } from './layouts/layouts.module';
import { ComponentsModule } from './components/components.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutsModule,
    ComponentsModule
  ],
  exports: [
    LayoutsModule,
    ComponentsModule
  ]
})
export class CoreModule { }
