# Desafio Front-End | Dashboard | Iuricode

Este repositório é uma solução para o **Desafio Front-End (N°40)** "Dashboard" disponibilizado pelo iuricode (Que pode ser encontrado [aqui](https://github.com/iuricode/desafios-frontend)), ao qual foi disponibilizado diversos assets (de ícones e logo) e um protótipo no Figma para o desenvolvimento.
A solução proposta foi totalmente desenvolvida em Angular, sem utilizar bibliotecas para gráficos, etc. O aplicativo foi criado utilizando conceitos como componentes, layouts e boas práticas de desenvolvimento. Utilizei também a biblioteca FakerJs para gerar alguns dados na aplicação.
Links:

 - Figma: [Link para o protótipo do Figma](https://www.figma.com/file/Yb9IBH56g7T1hdIyZ3BMNO/Desafios---Codel%C3%A2ndia?type=design&node-id=224375-16&mode=design&t=u8FHTHFEUhvLMv9D-0)
 - Deploy: [Link para deploy na Vercel](https://desafio-front-end-dashboard.vercel.app/)
